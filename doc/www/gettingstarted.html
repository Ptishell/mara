<!DOCTYPE html>
<html>
  <head>
    <title>Mara programming language - Getting started</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="icon" type="image/png" href="./res/img/favicon.png" />
    <link rel="stylesheet" href="./res/design/style.css" />
    <link rel="stylesheet" href="./res/design/highlight.css" />
  </head>
  <body>
    <header>
        <a href="./index.html">
          <img id="logo" alt="Mara" src="./res/img/logo.png" />
        </a>
    </header>
    <nav>
      <ul>
        <li>
          <div class='fl'>About</div>
          <ul>
            <li><a href="./index.html">Home</a></li>
            <li><a href="./examples.html">Examples</a></li>
            <li><a href="./faq.html">FAQ</a></li>
          </ul>
          <div class='fl'>Documentation</div>
          <ul>
            <li><a href="./gettingstarted.html">Getting started</a></li>
            <li><a href="./ref.html">Reference</a></li>
          </ul>
          <div class='fl'>Download</div>
          <ul>
            <li>
              <a href="http://git.psurply.com/mara/downloads/">
                Source code
              </a>
            </li>
            <li>
              <a href="http://git.psurply.com/mara/">
                Git repository
              </a>
            </li>
          </ul>
          <div class='fl'>Links</div>
          <ul>
            <li><a href="http://www.psurply.com/">My personal website</a></li>
            <li><a href="http://www.atmel.com/">Atmel website</a></li>
            <li><a href="http://www.nongnu.org/avr-libc/">avr-libc</a></li>
          </ul>
        </li>
      </ul>
    </nav>
    <div id="content">
      
<h1>Getting started with Mara</h1>
<h2>Setup the compiler</h2>
<h3>Dependencies</h3>
<p>
  Before building the compiler, make sure that the following programs is
  installed in your system :
</p>
<ul>
  <li><a href="http://caml.inria.fr/ocaml/index.en.html">OCaml</a></li>
  <li><a href="http://gallium.inria.fr/~fpottier/menhir/">Menhir</a></li>
  <li>ocamllex</li>
  <li>avr-gcc</li>
  <li><a href="http://www.nongnu.org/avr-libc/">avr-libc</a></li>
</ul>
<h3>Build the compiler</h3>
<pre>
  <code>
    $ cd /path/to/mara
    $ make
    # make install
  </code>
</pre>
<p>
  You can check if the compiler is correctly installed in your system by
  printing the version of your compiler with the following command :
</p>
<pre>
  <code>
    $ marac -v
    Mara compiler version X
    ...
  </code>
</pre>
<p>You are now able to try to write your first Mara program !</p>
<h3>Compile a Mara program</h3>
<p>
  To compile a Mara program, you need to specified on which microcontroller
  the program must be run and the main file of your project
  (without the <code>.mr</code> suffix). By default, the program is compiled
  for <code>atmega8</code>.
</p>
<pre>
  <code>
    $ marac -mmcu <em>MCU</em> <em>main_file</em>
  </code>
</pre>
<p>
  Then, the file <em>main_file</em><code>.hex</code> is produced and ready to
  get uploaded on the microcontroller with <code>avrdude</code>.
</p>
<h3>Using Mara with Arduino boards</h3>
<p>
  Mara is compatible with Arduino boards based on <em>AVR</em>.
  If you want to use the bootloader, the <code>.hex</code> file can be upload
  with the following command line :
</p>
<pre>
  <code>
    $ avrdude -p <em>MCU</em> -c arduino -b 57600 -P <em>DEVICE</em> -U flash:w:<em>FILENAME</em>.hex
  </code>
</pre>
<p>
  The <code>avrdude</code> <em>MCU</em> option is different from the
  Mara compiler <em>MCU</em> option. Refer to the following table to get the
  right <em>MCU</em> option in function of your Arduino board.
</p>
<table>
  <thead>
    <tr>
      <th>Board name</th>
      <th>MCU Mara</th>
      <th>MCU avrdude</th>
    </tr>
  </thead>
  <tbody>
    <tr><th>Arduino UNO</th><th>atmega328p</th><th>m328p</th></tr>
    <tr><th>Arduino Leonardo</th><th>atmega32u2</th><th>m32u2</th></tr>
    <tr><th>Arduino Mega 1280</th><th>atmega1280</th><th>m1280</th></tr>
    <tr><th>Arduino Mega 2560</th><th>atmega2560</th><th>m2560</th></tr>
  </tbody>
</table>
<h2>Structure of a Mara program</h2>
<p>
  A Mara program is always defined with a <code>setup</code> block and a
  <code>loop</code> block. The <code>setup</code> block is the entry point of
  the program, so the code contained on it is executed only once when the
  microcontroller is reset. This block is used to declare and initialize
  variables that the program needs. Once installation is complete, the code
  contained on the <code>loop</code> block is repeated until the microcontroller
  is shutdown.
  <br/>
  The end of a line marks the end of a statement, so unlike C, Mara does not
  require a semicolon at the end of each statement. You can include semicolons
  at the end of statements if you want to put several in the same line.
  Comments begin with a '#' and extend to the end of the line.
</p>
<img alt="Main structure" src="./res/img/graph/main.png" />
<h3>Example</h3>
<p>
  Let's try to blink a LED connected to the pin <code>PB5</code> of the
  microcontroller:
</p>
<pre>
<code>
<span class="structkw">setup</span>
  <span class="const">@DDRB</span> &lt;- DDB5                 <span class="comment"># Set PB5 as an ouput</span>

  <span class="const">@TCCR1B</span> &lt;- CS11 CS10          <span class="comment"># Select ClkIO / 64 (From prescaler) <span class="kw">for</span> Timer1</span>
  <span class="const">@TIMSK1</span> &lt;- TOIE1              <span class="comment"># Enable overflow <span class="structkw">interrupt</span></span>

  <span class="kw">sei</span>                           <span class="comment"># Enable global <span class="structkw">interrupt</span></span>
<span class="structkw">end</span>

<span class="structkw">interrupt</span> TIMER1_OVF            <span class="comment"># ISR Timer1 Overflow</span>
  <span class="const">@PORTB</span> ^^ PORTB5              <span class="comment"># Change PB5 <span class="structkw">state</span></span>
<span class="structkw">end</span>

<span class="structkw">loop</span>                            <span class="comment"># Infinite <span class="structkw">loop</span></span>
<span class="structkw">end</span>
</code>
</pre>

<p>See the <a href="./ref.html"> language reference</a> for more information.</p>

      <footer id="gbl">
        <p>
          &copy; 2013 Pierre Surply -
          <script type="text/javascript">
            document.write('<a href="mailto:'+String.fromCharCode(112,105,101,114,114,101,46,115,117,114,112,108,121,64,103,109,97,105,108,46,99,111,109)+'">Contact</a>');
          </script>
        </p>
      </footer>
    </div>
  </body>
</html>
