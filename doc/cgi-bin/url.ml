(*
** url.ml for OCaml-WebFramework
**
** Copyright (C) 2012 Pierre Surply
** <pierre.surply@gmail.com>
**
** This file is part of OCaml-WebFramework.
**
**    OCaml-WebFramework is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    OCaml-WebFramework is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
**
** Started on  Sun Sep 16 11:31:15 2012 Pierre Surply
**  Last update Sat Aug 24 18:48:28 2013 Pierre Surply
*)

let get_page_from_url () =
  match Env.get_url_arg 0 with
    | "ref"             ->
      "ref/" ^
        begin
          match Env.get_url_arg 1 with
          | "setup"     -> "setup.html"
          | "loop"	-> "loop.html"
          | "isr"	-> "isr.html"
          | "vardecl"   -> "vardecl.html"
          | "assign"    -> "assign.html"
          | "compound"  -> "compound.html"
          | "alt"  	-> "alt.html"
          | "while"  	-> "while.html"
          | "waitfor"  	-> "waitfor.html"
          | "for"  	-> "for.html"
          | "arith"     -> "arith.html"
          | "comp"      -> "comp.html"
          | "boolop"    -> "boolop.html"
          | "bitop"     -> "bitop.html"
          | "malloc"    -> "malloc.html"
          | "asm"       -> "asm.html"
          | "tern"      -> "tern.html"
          | "mcall"     -> "mcall.html"
          | "cast"      -> "cast.html"
          | "fsm"     	-> "fsm.html"
          | "c"     	-> "c.html"
	  | "type"	->
	    begin
	      match Env.get_url_arg 2 with
		| "integer"	-> "types/integer.html"
		| "cstring"	-> "types/cstring.html"
		| "vect"	-> "types/vect.html"
		| _     	-> "summary.html"
	    end
	  | "std"	->
	    begin
	      match Env.get_url_arg 2 with
		| "string"	-> "std/string.html"
		| "list"	-> "std/list.html"
		| _     	-> "summary.html"
	    end
	  | "oop"	->
	    begin
	      match Env.get_url_arg 2 with
		| "attr"	-> "oop/attr.html"
		| "methods"	-> "oop/methods.html"
		| "init"	-> "oop/init.html"
		| "inheritance"	-> "oop/inheritance.html"
		| _     	-> "summary.html"
	    end
          | _           -> "summary.html"
        end
    | "faq"             -> "faq.html"
    | "examples"        -> "examples.html"
    | "gettingstarted"  -> "gettingstarted.html"
    | _                 -> "index.html"

let make_page_from_url () =
  Template.make_page (get_page_from_url ())
