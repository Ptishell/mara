(*
** highlight.ml for Mara
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Mar 16 14:58:18 2013 Pierre Surply
**  Last update Thu Aug 22 20:47:08 2013 Pierre Surply
*)

let code_dir = "../res/code/"
let regex_struct_kw = Str.regexp
  begin
    "\\("                       ^
      "\\bclass \\b\\|"         ^
      "\\bsetup\\b\\|"          ^
      "\\bloop\\b\\|"           ^
      "\\bend\\b\\|"            ^
      "\\binterrupt\\b\\|"      ^
      "\\bfunc\\b\\|"           ^
      "\\bstate\\b\\|"          ^
      "\\baction\\b\\|"         ^
      "\\binput\\b\\|"          ^
      "\\bstatic\\b\\|"         ^
      "\\bfsm\\b\\|"            ^
      "\\bextern\\b\\|"         ^
      "\\bmethod\\b\\|"         ^
      "\\battr\\b\\|"           ^
      "\\busing\\b\\)"
  end
let regex_kw = Str.regexp
  begin
    "\\("                       ^
      "\\bif\\b\\|"             ^
      "\\bthen\\b\\|"           ^
      "\\belse\\b\\|"           ^
      "\\bendif\\b\\|"          ^
      "\\belif\\b\\|"           ^
      "\\bwhile\\b\\|"          ^
      "\\bwaitfor\\b\\|"        ^
      "\\bfor\\b\\|"            ^
      "\\bto\\b\\|"             ^
      "\\bdone\\b\\|"           ^
      "\\bdownto\\b\\|"         ^
      "\\bdo\\b\\|"             ^
      "\\breturn\\b\\|"         ^
      "\\band\\b\\|"            ^
      "\\bor\\b\\|"             ^
      "\\bnot\\b\\|"            ^
      "\\bbitls\\b\\|"          ^
      "\\bbitrs\\b\\|"          ^
      "\\bbitor\\b\\|"          ^
      "\\bbitand\\b\\|"         ^
      "\\bcompl\\b\\|"          ^
      "\\bnew\\b\\|"            ^
      "\\bdel\\b\\|"            ^
      "\\basm\\b\\|"            ^
      "\\bsei\\b\\|"            ^
      "\\bcli\\b\\|"            ^
      "\\bwdr\\b\\|"            ^
      "\\bsleep\\b\\|"          ^
      "\\bnop\\b\\|"            ^
      "\\bbreak\\b\\|"          ^
      "\\bvar\\b\\)"
  end
let regex_types = Str.regexp
  begin
    "\\("                       ^
      "\\binteger\\b\\|"        ^
      "\\bundef\\b\\|"          ^
      "\\bcstring\\b\\|"        ^
      "\\bstring\\b\\|"         ^
      "\\bvoid\\b\\|"           ^
      "\\bvect\\b\\|"           ^
      "\\blist\\b\\)"
  end
let regex_const = Str.regexp
  begin
    "\\("                       ^
      "\\bself\\|"              ^
      "\\btrue\\b\\|"           ^
      "\\bfalse\\b\\|"          ^
      "@[A-Z0-9:]+\\|"          ^
      "\\bnone\\b\\)"
  end
let regex_comment = Str.regexp "\\(#.*\\|\".*\"\\)"
let regex_lt = Str.regexp "<"
let regex_gt = Str.regexp ">"
let regex_lb = Str.regexp "\\["
let regex_rb = Str.regexp "\\]"

let span span s =
  "<span class=\"" ^ span ^ "\">" ^ (Str.matched_group 1 s) ^ "</span>"

let struct_kw s = span "structkw" s
let kw s        = span "kw" s
let types s     = span "types" s
let const s     = span "const" s
let comment s   = span "comment" s

let apply s =
  let rec r_load_template f =
    try
      let s = input_line f in
      let s = Str.global_replace regex_lt "&lt;" s in
      let s = Str.global_replace regex_gt "&gt;" s in
      let s_comment = Str.global_substitute regex_comment comment s in
      let s_struct_kw = Str.global_substitute regex_struct_kw struct_kw
        s_comment
      in
      let s_kw = Str.global_substitute regex_kw kw s_struct_kw in
      let s_types = Str.global_substitute regex_types types s_kw in
      let s_const = Str.global_substitute regex_const const s_types in
      let s = Str.global_replace regex_lb "&#91;" s_const in
      let s = Str.global_replace regex_rb "&#93;" s in
      s ^ "\n" ^ r_load_template f
    with End_of_file -> close_in f; ""
  in
  let file = open_in (code_dir ^ Str.matched_group 1 s) in
  "<pre>\n<code>\n" ^ r_load_template file ^ "</code>\n</pre>\n"
