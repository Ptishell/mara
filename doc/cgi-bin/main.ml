(*
** main.ml for OCaml-WebFramework
**
** Copyright (C) 2012 Pierre Surply
** <pierre.surply@gmail.com>
**
** This file is part of OCaml-WebFramework.
**
**    OCaml-WebFramework is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    OCaml-WebFramework is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
**
** Started on  Sat Sep 15 11:14:18 2012 Pierre Surply
**  Last update Mon Jul 15 12:15:15 2013 Pierre Surply
*)

let content_type = "Content-Type: text/html;\n\n"

let main () =
  if Array.length Sys.argv > 1 &&
    Sys.argv.(1) = "static"
  then
    print_string (Url.make_page_from_url ())
  else
    print_string (content_type ^
		    Url.make_page_from_url ());
  exit 0

let _ = main ()
