(*
** changes.ml for Mara
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Mar 15 15:28:44 2013 Pierre Surply
**  Last update Wed Aug 14 11:24:02 2013 Pierre Surply
*)

let regexul = Str.regexp "^R\\(.*\\) \\(.*\\)"
let regexli = Str.regexp "- \\(.*\\)"

let first = ref true

let ul s =
  (if !first then (first := false; "")  else "</ul>\n") ^
    "<h3>Revision " ^
    (Str.matched_group 1 s) ^
    " (" ^ (Str.matched_group 2 s) ^ ")</h3>\n" ^
    "<ul>"

let li s =
  "<li>" ^ (Str.matched_group 1 s) ^ "</li>"

let changelog () =
  let rec r_load_template f =
    try
      let s = input_line f in
      let s_uled = Str.global_substitute regexul
        ul s in
      let s_lied = Str.global_substitute regexli
        li s_uled in
      s_lied ^ "\n" ^ r_load_template f
    with End_of_file -> close_in f; ""
  in
  let file = open_in "../CHANGES" in
  r_load_template file ^ "</ul>"
